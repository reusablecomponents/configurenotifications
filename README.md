Add "UserNotifications" framework in your project

In AppDelagte class

Step 1: In your AppDelegate.swift file, import the UserNotifications framework at the top.

write import UserNotifications

Step 2: In the applicationDidFinishLaunchingWithOptions( ) method, specify which things you want to request authorizations for by calling UNUserNotificationCenter.current( ).requestAuthorization(:). In the options parameter, specify what options you want to include (alert? sound? badge?) A badge looks in red circle in the top right corner of the icon:


Step 4: In the ViewController.swift file, import the UserNotifications framework.
