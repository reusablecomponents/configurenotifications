//
//  ViewController.swift
//  InAppNotifications
//
//  Created by Sakshi on 10/05/18.
//  Copyright © 2018 Signity Software Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import UserNotifications


class ViewController: UIViewController {
    
    @IBAction func sendNotification(_ sender: UIButton) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date(timeIntervalSinceNow:2*60)
        
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second], from: date)
        
        let trigger:UNCalendarNotificationTrigger =  UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder"
        content.body = "Car Wash:Take car to garage"
        if let path = Bundle.main.path(forResource: "ping_me", ofType: "png") {
            let url = URL(fileURLWithPath: path)
            do {
                let attachment = try UNNotificationAttachment(identifier: "ping_me", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
            
            let request = UNNotificationRequest(identifier: content.title, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error) in
                print("error")
            })
            
        }
        
    }
}

